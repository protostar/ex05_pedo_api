import express from "express";
const PORT = 3000;

const app = express();

app.set("json spaces", 4);
app.use(express.static("public"));

/**
* @api {get} / API status
* @apiGroup Status
* @apiSuccess {String} status API status Message
* @apiSuccessExample {json} success
*   HTTP /1.1 200 OK
*   {"status": "NTask API"}
*/
app.get("/", (req, res) => res.json({status: "NTask API"}));

/**
 * @api {get} /pedoapiresult/:n1?/:n2? Without parametres prints 100 positions. With parameters print within this range.
 * @apiGroup Exercise
 * @apiParam {Number} [n1] One of the parameters that correspond to a range of numbers to print.
 * @apiParam {Number} [n2] One of the parameters that correspond to a range of numbers to print.
 * @apiSuccess {Number} AnyInteger Corresponds to a number printed on the screen
 * @apiSuccess {String} Pe Corresponds to a multiple of three
 * @apiSuccess {String} Do Corresponds to a multiple of five
 * @apiSuccess {String} PeDo Corresponds to a multiple of three and five
 * @apiSuccessExample {json} Success
 *    HTTP/1.1 200 OK
 *    {
      "result": [
          1,
          2,
          "Pé",
          4,
          "Do",
          "Pé",
          7,
          8,
          "Pé",
          "Do",
          11,
          "Pé",
          13,
          14,
          "PéDo",
          ...
      ]
  }
 * @apiErrorExample {json} Error
 *    HTTP/1.1 422 Unprocessable Entity
 *    {
 *      "result": "Numbers can not be the same!"
 *    }
 */
app.get("/pedoapiresult/:n1?/:n2?", (req, res) => {

  let n1     = parseInt(req.params.n1);
  let n2     = parseInt(req.params.n2);
  let nStart = null;
  let nEnd   = null;
  let result = [];

  if (isNaN(n1) || isNaN(n2)) {
    nStart = 1;
    nEnd   = 100;
  } else if(n1 > n2){
    nStart = n2;
    nEnd   = n1;
  } else if(n2 > n1) {
    nStart = n1;
    nEnd   = n2;
  } else if(n2 == n1) {
    res.status(422).json({result: 'Numbers can not be the same!'});
    return res.end();
  }

  for (let i = nStart; i <= nEnd; i++) {
    if(i % 3 == 0 && i % 5 == 0) {
      result.push('PéDo');
    } else if (i % 3 == 0) {
      result.push('Pé');
    } else if(i % 5 ==  0) {
      result.push('Do');
    } else {
      result.push(i);
    }
  }

  res.json({result});
});

app.listen(PORT, () => console.log(`NTask API = porta ${PORT}`));
