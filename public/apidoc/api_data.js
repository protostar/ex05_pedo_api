define({ "api": [
  {
    "type": "get",
    "url": "/pedoapiresult/:n1?/:n2?",
    "title": "Without parametres prints 100 positions. With parameters print within this range.",
    "group": "Exercise",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "n1",
            "description": "<p>One of the parameters that correspond to a range of numbers to print.</p>"
          },
          {
            "group": "Parameter",
            "type": "Number",
            "optional": true,
            "field": "n2",
            "description": "<p>One of the parameters that correspond to a range of numbers to print.</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "Number",
            "optional": false,
            "field": "AnyInteger",
            "description": "<p>Corresponds to a number printed on the screen</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Pe",
            "description": "<p>Corresponds to a multiple of three</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "Do",
            "description": "<p>Corresponds to a multiple of five</p>"
          },
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "PeDo",
            "description": "<p>Corresponds to a multiple of three and five</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "Success",
          "content": " HTTP/1.1 200 OK\n {\n    \"result\": [\n        1,\n        2,\n        \"Pé\",\n        4,\n        \"Do\",\n        \"Pé\",\n        7,\n        8,\n        \"Pé\",\n        \"Do\",\n        11,\n        \"Pé\",\n        13,\n        14,\n        \"PéDo\",\n        ...\n    ]\n}",
          "type": "json"
        }
      ]
    },
    "error": {
      "examples": [
        {
          "title": "Error",
          "content": "HTTP/1.1 422 Unprocessable Entity\n{\n  \"result\": \"Numbers can not be the same!\"\n}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "rotes/index.js",
    "groupTitle": "Exercise",
    "name": "GetPedoapiresultN1N2"
  },
  {
    "type": "get",
    "url": "/",
    "title": "API status",
    "group": "Status",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "String",
            "optional": false,
            "field": "status",
            "description": "<p>API status Message</p>"
          }
        ]
      },
      "examples": [
        {
          "title": "success",
          "content": "HTTP /1.1 200 OK\n{\"status\": \"NTask API\"}",
          "type": "json"
        }
      ]
    },
    "version": "0.0.0",
    "filename": "rotes/index.js",
    "groupTitle": "Status",
    "name": "Get"
  }
] });
